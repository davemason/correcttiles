//@String(label = "X tiles",value="3") tx
//@String(label = "Y tiles",value="3") ty
//@String(label = "Channel?",value="3") chan
//@Boolean(label = "Correct Intensity?",description="For uneven samples (for instance tissue plus background) check the box to correct intensity") doCorrection

//-- Make sure there's an image open
if (nImages()<1){exit("ERROR: No images open!");}

//-- Check that the channel is valid
if (chan>nSlices()){exit("ERROR: Selected channel out of range!");}

//-- Find the dimensions of the tiled image
getDimensions(w, h, c, s, f);

//-- Calculate the individual image size
image_x=w/tx;
image_y=h/ty;

if (doCorrection==true){

//-- Split the transmitted channel, then de-montage and process as a stack
title=getTitle();
rename("original");
if (nSlices>1){
	run("Duplicate...", "title=trans duplicate channels="+chan);
}

//-- Break up into a stack (note stack order is row by row from top left)
run("Montage to Stack...", "images_per_row="+tx+" images_per_column="+ty+" border=0");
run("Bandpass Filter...", "filter_large="+floor(image_x*0.1)+" filter_small=0 suppress=None tolerance=5 process");

//-- Correct the intensity across the stack
run("Bleach Correction", "correction=[Simple Ratio] background=0");
run("Make Montage...", "columns="+ty+" rows="+tx+" scale=1");

//-- Tidy up windows
close(title);
close("Stack");
close("DUP_Stack");
selectWindow("Montage");

isOpen("trans"){
	//-- this was part of a multichannel image so replace original slice
	run("Copy");
	selectWindow("original");
	setSlice(chan);
	run("Paste");
	run("Select None");
	close("trans");
	close("Montage");
	
}
rename(title);

} else {

//-- Run the FBP filter on each original image without correction
for (row=0;row<ty;row++){
	
	for (col=0;col<tx;col++){
	
		run("Specify...", "width="+image_x+" height="+image_y+" x="+(col*image_x)+" y="+(row*image_y)+" slice="+chan);
		run("Bandpass Filter...", "filter_large="+floor(image_x*0.1)+" filter_small=0 suppress=None tolerance=5");

	}//-- column

}//-- row

run("Select None");
resetMinAndMax();

} //-- Close correction loop

//-- close the log if open
if (isOpen("Log")) {selectWindow("Log");run("Close");} 
