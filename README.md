### Correcting uneven illumination in a tile scan ###
A simple [Fiji](http://fiji.sc) script to correct uneven transmitted images in stitched tile scans.

Some software auto-stitch tiled acquisitions which can be a pain if you want to flat-field correct images or do something to the individual parts. This script iterates through the tiles and processes (in this case) with a bandpass filter in Fourier space.

![watermark](example.gif)

### Update ###
After some helpful feedback on Twitter, I've added an option to correct the intensity when the sample is uneven. Originally, if the intensity histogram were quite different (for example if you have tissue and background - see below left) the BP filter skews the intensity of the individual tiles (below middle). You can now check the box to run an intensity normalisation on each of the tiles, which should go most of the way to fixing the problem (below right).

![watermark](corrected.png)

### Acknowledgement ###
written by [Dave Mason](http://pcwww.liv.ac.uk/~dnmason), from the University of Liverpool [Centre for Cell 
Imaging](http://cci.liv.ac.uk).

Thanks to [Lindsey Peng](https://twitter.com/lqpeng) and the [CDB Microscopy Core](https://twitter.com/CDBMicroCore) for feedback.